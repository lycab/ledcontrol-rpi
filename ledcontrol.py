import RPi.GPIO as gp
import time
import threading
import os, sys

if not os.getuid() == 0:
	print "You are not root"
	sys.exit("Exiting")

"""Pin Numbers used by Me (blaxxun/lysiac)"""
red = 13
green = 8
blue = 11
dilat = 0.01

class LedControl:
	"""Class to control RGBLed on a Raspberrypi
	   Requirements: RPi.GPIO
			 time
			 threading
			 os
	"""

	
	def __init__(self, red, green, blue):
		"""Initialize: Set GPIO preferences, initialize 3 Pins with Low and set values for colors """
		gp.setwarnings(False)
		gp.setmode(gp.BOARD)
		gp.setup(red, gp.OUT, initial=gp.LOW)
		gp.setup(green, gp.OUT, initial=gp.LOW)
		gp.setup(blue, gp.OUT, initial=gp.LOW)
		self.red = red
		self.green = green
		self.blue = blue
		self.colStat = { red:False, green:False, blue:False}

	def set(self, color, state=True):
		"""Set state of Pins aka colors"""
		self.colStat[color] = state
		gp.output(color, state)
	def unset(self, color):
		"""Turn color pin off (actually this is a wrapper for self.set(color, state=False))"""
		self.set(color, state=False)

	def off(self):
		"""Set all 3 Pins to LOW aka Led completely off"""
		self.set(self.red, False)
		self.set(self.green, False)
		self.set(self.blue, False)
		self._stop = True

	def toggle(self, color):
		"""Toggle Pin aka color"""
		self.colStat[color] = not self.colStat[color]
		gp.output(color, self.colStat[color])
	
	def blinkT(self, color, frequency, duration, num):
		"""Blink Thread funktion - Not to be used directly"""
		if num == 0:
			while not self._stop:
				self.set(color, True)
				time.sleep(float(duration) * 1/(float(2*frequency)))
				self.set(color, False)
				time.sleep(( 1 - duration/2. ) * 1/(float(frequency)))
		else:
			for i in range(num):
				self.set(color, True)
				time.sleep(float(duration) * 1/(float(2*frequency)))
				self.set(color, False)
				time.sleep(( 1 - duration/2. ) * 1/(float(frequency)))


	def blink(self, color, frequency, duration=1., st=True, num = 0):	
		"""Blink management function
		   duration is to be a float value between 0 and 1, it defines
		   the duration of an half period in which the led glows"""

		if st:
			self._stop = False
			blinkThread = threading.Thread(target = self.blinkT, args =(color, frequency, duration, num) )
			blinkThread.start()
		if not st:
			self._stop = True
			self.off()
	
	def rgb(self, redT, greenT, blueT, timeT):
		"""NOT WORKING PROPERLY
		   Set The Led pins on for specific times to emulate a pwm and get rgb colors"""
		redtime = float( redT * timeT / ( redT + greenT + blueT ) )
		greentime = float( greenT * timeT / ( redT + greenT + blueT ) )
		bluetime = float( blueT * timeT / ( redT + greenT + blueT ) )
		self._stop = False

		self.set(self.red, False)
		self.set(self.green, False)
		self.set(self.blue, False)
		
		while True:
			self.toggle(self.red)
			time.sleep(redtime)
			self.toggle(self.red)
			self.toggle(self.green)
			time.sleep(greentime)
			self.toggle(self.green)
			self.toggle(self.blue)
			time.sleep(bluetime)
			self.toggle(self.blue)
			
			if self._stop:
				break

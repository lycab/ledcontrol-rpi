#LedControl - RPI

LedControl is a simple python class (packaged into a module) to control
Leds attached to the GPIO Pins of a Raspberrypi.

It is basically nothing else than a wrapper for the gpio control.
